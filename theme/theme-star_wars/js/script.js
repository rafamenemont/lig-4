// fazer tabuleiro dinamicamente
function makeTable(){
    for (let i = 0; i < 7; i++) {
    let col = document.createElement("div")
    col.style.display = "flex";
    col.style.flexDirection = "column";
    col.dataset.local = i

    let table = document.getElementById('table')

    for (let cel = 0; cel < 6; cel++) {
        let celula = document.createElement("div")
        celula.dataset.local = i + "-" + cel

        celula.classList.add("celStyle");
        col.appendChild(celula)
    }
    table.appendChild(col)
}

}
makeTable()




//Inicia outra partida trocando a cor de quem começa

let turn = 0;

function newTurn () {
    
let blackWinner = document.querySelector(".blackWins");
let redWinner = document.querySelector(".redWins");
let tie = document.querySelector(".tie")

table.addEventListener("click", makeMove);

    tie.style = "display:none";
    blackWinner.style = 'display:none'
    redWinner.style = 'display:none'
    table.innerText = ''
    makeTable()
    sumRound = 0
    turn++
    sumRound =+ turn
}

//função que cria as bolinhas

let sumRound = 0;

function makeBalls(position) {
    sumRound++;
    const redBall = document.createElement('div');
    redBall.classList.add("redBall");

    const blackBall = document.createElement('div');
    blackBall.classList.add("blackBall");

   
        if (sumRound % 2 === 1) {
            position.appendChild(redBall);
        } else {
            position.appendChild(blackBall);
        
    }
};

// Função reset para botão 

let reloadPage = document.getElementById("reset");
reloadPage.addEventListener("click", () => location.reload());




// Adicionando as bolinhas no tabuleiro

const table = document.getElementById("table");

table.addEventListener("click", makeMove);

function makeMove(event) {
 
    let collumn = event.target.parentNode

    if(collumn.children.length === 6){

        if (collumn.children[5].children.length === 0) {
            makeBalls(collumn.children[5])
        }

        else if (collumn.children[4].children.length === 0) {
            makeBalls(collumn.children[4])
        }

        else if (collumn.children[3].children.length === 0) {
            makeBalls(collumn.children[3])
        }

        else if (collumn.children[2].children.length === 0) {
            makeBalls(collumn.children[2])
        }

        else if (collumn.children[1].children.length === 0) {
            makeBalls(collumn.children[1])
        }

        else if (collumn.children[0].children.length === 0) {
            makeBalls(collumn.children[0])
        }
        whoTurns ()
        victory() 
    }
}

//função da condição de vitória ou empate

function victory() {
    let arrVic = [
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0]
     ]
 
    for(let x=0; x<7; x++){
         for(let y=0; y<6; y++){
             let parent = document.querySelector(`div[data-local='${x}-${y}']`)
 
             let child = parent.childNodes
             child = child[0]
             if(child === undefined){
                 arrVic[x][y] = 0
             } else if(child.className === "redBall" ){
                 arrVic[x][y] = 1
             } else if(child.className === "blackBall"){
                 arrVic[x][y] = 2
             }
         }
     }
 
 const edgeX = arrVic[0].length - 3;
 const edgeY = arrVic.length - 3;
 
 // HORIZONTAL
 for(let y = 0; y < arrVic.length; y++){
   for(let x = 0; x < edgeX; x++) {
     let cell = arrVic[y][x];
 
     if(cell !== 0) {
       
       if(cell === arrVic[y][x+1] 
       && cell === arrVic[y][x+2] 
       && cell === arrVic[y][x+3]) {
         if(cell === 1){
             showVictory("red")
         }else {
             showVictory("black")
         }
       }
     }
   }
 }
 
 // VERTICAL
 for(let y = 0; y < edgeY; y++){
 
   for(let x = 0; x < arrVic[0].length; x++) {
     cell = arrVic[y][x];
     
     if(cell !== 0) {
       
       if(cell === arrVic[y+1][x] 
       && cell === arrVic[y+2][x]
       && cell === arrVic[y+3][x] ) {
         if(cell === 1){
             showVictory("red")
         }else {
             showVictory("black")
         }
       }
     }
   }
 }
 
 // DIAGONAL (DOWN RIGHT) 
 for(let y = 0; y < edgeY; y++){
 
   for(let x = 0; x < edgeX; x++) {
     cell = arrVic[y][x];
     
     if(cell !== 0) {
       
       if(cell === arrVic[y+1][x+1] 
       && cell === arrVic[y+2][x+2]
       && cell === arrVic[y+3][x+3] ) {
         if(cell === 1){
             showVictory("red")
         }else {
             showVictory("black")
         }
       }
     }
   }
 }
 
 
 // DIAGONAL (DOWN LEFT)
 for(let y = 3; y < arrVic.length; y++){
 
   for(let x = 0; x < edgeX; x++) {
     cell = arrVic[y][x];
     
     if(cell !== 0) {
       
       if(cell === arrVic[y-1][x+1] 
       && cell === arrVic[y-2][x+2]
       && cell === arrVic[y-3][x+3] ) {
         if(cell === 1){
             showVictory("red")
         }else {
             showVictory("black")
         }
       }
     }
   }
 }

 // TIE

 let plays = 0;

 for(let i = 0; i < arrVic.length; i++) {
     
    for(let j = 0; j < arrVic[i].length; j++) {
        plays += arrVic[i][j];
    
        if(plays === 63){
            whoNewTurns ()
            showVictory("tie") 
            table.removeEventListener("click", makeMove);
        }
    }

 }

  
}

//função para exibir quem ganhou e perguntando se quer jogar novamente

let redScore = document.getElementById('redScore')
let blackScore = document.getElementById('blackScore')
let redVictory = 0
let blackVictory = 0

function showVictory(color) {

    let redPlayer = document.querySelector(".redWins");
    let blackPlayer = document.querySelector(".blackWins");
    let tie = document.querySelector(".tie")
 
    table.removeEventListener("click", makeMove);
    if (color === "red") {
        
        redPlayer.style = "display:flex"
        redVictory++
        redScore.innerText = `Score: ${redVictory}`
    }

    

    else if(color === "black") {
        
        blackPlayer.style = "display:flex"
        blackVictory++
        blackScore.innerText = `Score: ${blackVictory}`

    }
    else{
        tie.style = "display:flex"

    }
}

// função que mostra de quem é a jogada

function whoTurns () {
    
    let blackScore = document.querySelector(".scoreBlack")
    let redScore = document.querySelector(".scoreRed")
    let redScoreText = document.querySelector(".scoreTextRed")
    let blackScoreText = document.querySelector(".scoreTextBlack")


    if(sumRound % 2 === 0) {
        blackScore.classList.remove("blackTurn")
        redScore.classList.add("redTurn")

        
        redScoreText.innerHTML = "Dark turn"    
    }
    else{
       
        redScore.classList.remove("redTurn")
        blackScore.classList.add("blackTurn")

        
        blackScoreText.innerHTML = "Jedi turn"
    }

}

function whoNewTurns () {
    let blackScore = document.querySelector(".scoreBlack")
    let redScore = document.querySelector(".scoreRed")

    if(turn % 2 === 1) {
        blackScore.classList.remove("blackTurn")
        redScore.classList.add("redTurn")
        redScore.innerHTML = "Red player's Turn"
    }
    else{
        
        redScore.classList.remove("redTurn")
        blackScore.classList.add("blackTurn")
    }
}

//adcionando eventListener aos botões rules e play 

const play = document.getElementById('play')
const rules = document.getElementById('rules')
const instructions = document.getElementById('instructions')
const closeRules = document.getElementById('closeRules')


play.addEventListener('click', function () {

const score = document.getElementById("score");

if (score.style.display === "flex") {
    score.style = "display: none";
    }
    else{
        score.style = "display: flex";
    }


    if (table.style.display === "flex") {
    table.style = "display: none";
    }
    else{
        table.style = "display: flex";
    }
});

rules.addEventListener('click', function () {

    table.classList.add('hidden')
    instructions.classList.remove('hidden')
})

closeRules.addEventListener('click', function(){
    instructions.classList.add('hidden')

})


//Criando EventListner para o 'play again' que aparece quando há uma vitória

let playagain1 = document.getElementById('playagain1')
let playagain2 = document.getElementById('playagain2')
let playagain3 = document.getElementById('playagain3')




playagain1.addEventListener('click',newTurn);
playagain2.addEventListener('click', newTurn);
playagain3.addEventListener('click', newTurn);









let music = document.getElementById('bb8Music');
let bb8Audio = new Audio('../music/Star Wars- The Imperial March (Darth Vader s Theme).mp3');
    
bb8Audio = true;



let btnTheme = document.getElementById('btnTheme');

btnTheme.addEventListener('click', ()=>{
    window.location = '../theme-rickAndMorty';
});