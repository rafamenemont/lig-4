=======================================================================================================================
###development status: ⚠️ in progress
<p>The project's proposal is to work as a team and develop soft skills. This project is being developed by 4 dev's Q1</p>
1. Amanda Medeiros
2. Fernando Mós
3. Leomar Romanzini
4. Rafael Monteiro

#How to play
<p>We have two teams, the red and the black. When it is your turn on the scoreboard it will be your turn to play. "Black's Turn" or "Red's Turn".
After that moment you must select one of the columns where you will play your ball.
With each movement we will have a shift change.</p>

<p>A solved game is a game whose result (winning👍, losing👎 or drawing🤝) can be predicted correctly from any position, assuming that both players play perfectly.</p>
<p>Winning conditions: a sequence of four pieces in a row. It can be vertically, horizontally and diagonally.</p>
<p>If none of these conditions are met, we will have the condition of a tie.</p>

#Technologies used
<table>
    <tr>
    <td>HTML</td>
    <td>CSS</td>
    <td>JavaScrpit</td>
    </tr>
    <tr>
    <td>5</td>
    <td>3</td>
    <td>ES6</td>
    </tr>
</table>
<p> This design is compatible with most devices and most resolutions. </p>
